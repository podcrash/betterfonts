## Minecraft TrueType/OpenType Font Support Mod ##

The BetterFonts mod adds TrueType/OpenType font support for Minecraft. This mod will use the fonts installed on your system for drawing text instead of the builtin bitmap fonts that come with Minecraft. Languages such as Arabic and Hindi look much better with this mod since both require complex layout that the bitmap fonts simply can't provide. All in-game text will change to use the new fonts including GUIs, the F3 debug screen, chat, and even signs. This mod should have little or no impact on performance.

**Unicode Text in Chat:**
Starting with 1.3.2, both the regular Minecraft server and CraftBukkit allow using full Unicode text in chat. Unfortunately, the Minecraft client comes with an older version of the LWJGL library which doesn't handle keyboard layouts properly. You will have to [manually update LWJGL](http://www.minecraftwiki.net/wiki/LWJGL) to the latest version, if you need Unicode support in chat.

### Forum & Downloads: ###
For more information, discussions, and downloadable versions of the mod, see the [BetterFonts Thread](http://www.minecraftforum.net/topic/1142084-125-betterfonts-opentype-font-support/) on the Minecraft Forums.

### Screenshots: ###
Click on any of the screenshots below to see a full-sized version of the image.

|																																							|																																						|
|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
|![English Title Screen](http://lh5.googleusercontent.com/-BSVTO520XtM/T30lcUsatWI/AAAAAAAAAzg/ruUVL9SL2_M/s853/title.png =400x225)English Title Screen		|![Arabic Options](http://lh6.googleusercontent.com/-o1UQZEMqGoE/T33uk5tV4SI/AAAAAAAAA0c/sgPPCUxefzM/s851/options.png =400x225)Options Screen in Arabic	|
|![Language Selection](http://lh3.googleusercontent.com/-tKPfGw-A-bE/T33uk3ouJ0I/AAAAAAAAA0Y/xs25h0QEgdc/s851/lang.png =400x225)Language Selection Screen	|![F3 Debug Screen](http://lh3.googleusercontent.com/-DeTb7J-ipGc/T30leXP8mxI/AAAAAAAAAz4/7MmCAuEUrCI/s852/debug.png =400x225)F3 Debug Screen			|

### Installation: ###
Installation is the same as for any other mod. Extract the contents of the downloaded .zip file into your minecraft.jar, and make sure to delete the META-INF folder inside minecraft.jar. This mod is compatible with OptiFine and MCPatcher, but make sure to install it after either mod.

### License: ###
This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but **WITHOUT ANY WARRANTY**; without even the implied warranty of MERCHANTABILITY or **FITNESS FOR A PARTICULAR PURPOSE**. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <<http://www.gnu.org/licenses/>>.
